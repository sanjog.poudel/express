// generate token (id card)
//verify token (id card verify)

//generate token => infoObj,logo,expiryInFO =>infoObj,secretInfo,expiryInFO
//json web token (jwt)

import jwt from "jsonwebtoken";
let infoObj = {
  id: "1234",
};
export let secretKey = "dw12";
let expiryInFO = {
  expiresIn: "365d",
}; //expireInfo must be in same format

// let token = jwt.sign(infoObj, secretKey, expiryInFO);
// console.log(token);

let myToken =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQiLCJpYXQiOjE3MDk3MTgzMDMsImV4cCI6MTc0MTI1NDMwM30.oknkrH1LMiNPVMXNZA-9KiKZCsg8oTNwb_hlA4LCzX8";

try {
  let infoObj1 = jwt.verify(myToken, "dw12");
  console.log(infoObj1);
} catch (error) {
  console.log(error.message);
}
