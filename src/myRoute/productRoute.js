import { Router } from "express";
import {
  createProductController,
  deleteProductController,
  readAllProductController,
  readSpecificProductController,
  updateProductController,
} from "../controller/productController.js";
import Joi from "joi";

/* let canAddProduct = (age) => {
  return (req, res, next) => {
    if (age > 18) {
      next();
    } else if (age < 18) {
      res.status(400).json({
        success: false,
        message: "you are not allowed to create product",
      });
    }
  };
}; */
/* let canAddProduct = (age, married) => {
  return (req, res, next) => {
    if (age > 20 && married === true) {
      next();
    } else if (age < 20 && married === false) {
      res.status(400).json({
        success: false,
        message: "you are not allowed to create product",
      });
    }
  };
};*/
let productRouter = Router();
let productValidation = Joi.object()
  .keys({
    name: Joi.string(),
    roll: Joi.number(),
    isMarried: Joi.boolean(),
  })
  .unknown(true);
productRouter
  .route("/")
  // .post(canAddProduct("a"), createProductController)
  /* 
  if a middleware pass a age and if age < 18 he can create product else throw error
  make middleware pass age, status 
  if age is greater than 20 and status is married , he can create product else throw error
  */
  // .post(canAddProduct("age"), createProductController)
  // .post(canAddProduct("age", "Married"), createProductController)
  .post((req, res, next) => {
    let data = productValidation.validate(req.body);

    let error = data.error;
    if (error) {
      res.json({
        success: false,
        message: error.details[0].message,
      });
    } else {
      next();
    }
  }, createProductController)
  .get(readAllProductController);

productRouter
  .route("/:id")
  .get(readSpecificProductController)
  .patch(updateProductController)
  .delete(deleteProductController);
export default productRouter;
