import { Router } from "express";
import { uploadMultipleFile, uploadSingleFile } from "../controller/fileController.js";
import upload from "../utils/uploadFile.js";

let fileRouter = Router();
fileRouter.route("/single").post(upload.single("file1"), uploadSingleFile);

fileRouter
  .route("/multiple")
  .post(upload.array("file1"), uploadMultipleFile);

export default fileRouter;
