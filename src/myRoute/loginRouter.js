import { Router } from "express";
import {
  LoginUser,
  createLogin,
  deleteLogin,
  readAllLogin,
  readSpecificLogin,
  updateLogin,
  verifyEmail,
} from "../controller/loginController.js";

let loginRouter = Router();
loginRouter.route("/").post(createLogin).get(readAllLogin);
loginRouter.route("/verify-email").patch(verifyEmail);
loginRouter.route("/login").post(LoginUser);

loginRouter
  .route("/:id")
  .get(readSpecificLogin)
  .patch(updateLogin)
  .delete(deleteLogin);
export default loginRouter;
