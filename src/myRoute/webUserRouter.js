import { Router } from "express";
import { createWebUSerController } from "../controller/webUserController.js";

let webUserRouter = Router();
webUserRouter
  .route("/")
  .post(createWebUSerController)

  .get();

webUserRouter.route("/: webUserId").get().patch().delete();

export default webUserRouter;
