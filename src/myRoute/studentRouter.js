import { Router } from "express";
import {
  createStudent,
  deleteStudent,
  readAllStudent,
  readSpecificStudent,
  updateStudent,
} from "../controller/studentController.js";

import validation from "../../middleware/validation.js";
import Joi from "joi";

let studentRouter = Router();

//.string
//value must be string
//it should not be empty
//.min(3) => the field must have at least 3 character
//.max(10) => the field must not have more than 10 character

// .number
//value must be number(it does not see type)
//it means 21 and "21" are same
//.min()
//.max()

//.boolean
//value must be boolean

//required => any(string,number,boolean)
//you must pass field

// enum => fixed value (male, female , other)
// .valid("male","female","other")

//through custom error

//object
// Joi.object().keys({
//   ...
// })

//array
// Joi.array().items()

// .when()

let studentValidation = Joi.object()
  .keys({
    name: Joi.string().required().min(3).max(10).messages({
      "any.required": "name is required",
      "string.base": "field must be string",
      "string min": "name must be at least 3 character",
      "string.max": "name must be at most 10 character",
    }),
    age: Joi.number()
      .required()
      // .min(18).max(60),
      .custom((value, msg) => {
        if (value >= 18) {
          return true;
        } else {
          return msg.message("age must be at least 18");
        }
      })
      .messages({
        "any.required": "age is required",
        "number.base": "field must be string",
      }),
    isMarried: Joi.boolean().required().messages({
      "any.required": "isMarried is required",
      "boolean.base": "field must be boolean",
      "boolean.empty": "Value cannot be empty.",
    }),
    spouseName: Joi.when("isMarried", {
      is: true,
      then: Joi.string().required(),
      otherwise: Joi.string(),
    }).messages({
      "any.required": "name is required",
      "string.base": "field must be string",
    }),

    // if married = true =>spouseName => required
    // if married = false =>spouseName is not required

    gender: Joi.string().required().valid("male", "female", "other").messages({
      "string.base": "gender must be string",
      "any required": "gender is required",
      "any.only": "gender must be either male or female or other",
    }),
    email: Joi.string()
      .required()
      .custom((value, msg) => {
        let validEmail = value.match(
          /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
        );
        if (validEmail) {
          return true;
        } else {
          return msg.message("email is invalid");
        }
      }),
    password: Joi.string()
      .required()
      .custom((value, msg) => {
        let isValidPassword = value.match(
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()-_+=])[A-Za-z\d!@#$%^&*()-_+=]{8,15}$/
        );

        if (isValidPassword) {
          return true;
        } else {
          return msg.message(
            "password must have at least one uppercase, one lowercase, one number,one symbol,min 8 character and max 15 character"
          );
        }
      }),

    phoneNumber: Joi.number().required().messages({
      "any.required": "number is required",
      "number.base": "field must be number",
    }),
    dob: Joi.string().required().messages({
      "any.required": "string is required",
      "string.base": "field must be string",
    }),
    location: Joi.object().keys({
      country: Joi.string().required().messages({
        "any.required": "country is required",
        "string.base": "filed must be string",
      }),
      exactLocation: Joi.string().required().messages({
        "any.required": "exactLocation is required",
        "string.base": "filed must be string",
      }),
    }),
    favTeacher: Joi.array().items(Joi.string().required()).messages({
      "any.required": "favTeacher is required",
      "string.base": "filed must be string",
    }),

    favSubject: Joi.array().items(
      Joi.object().keys({
        bookName: Joi.string().required().messages({
          "any.required": "bookName is required",
          "string.base": "filed must be string",
        }),
        bookAuthor: Joi.string().required().messages({
          "any.required": "bookAuthor is required",
          "string.base": "filed must be string",
        }),
      })
    ),
  })
  .unknown(true);

studentRouter
  .route("/")
  .post(validation(studentValidation), createStudent)
  .get(readAllStudent);

studentRouter
  .route("/:id")
  .get(readSpecificStudent)
  .patch(updateStudent)
  .delete(deleteStudent);
export default studentRouter;
