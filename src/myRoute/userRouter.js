import { Router } from "express";
import {
  createUserController,
  deleteUserController,
  loginUserController,
  readAllUserController,
  readSpecificUserController,
  updateUserController,
} from "../controller/userController.js";

let userRouter = Router();
userRouter.route("/").post(createUserController).get(readAllUserController);

userRouter
  .route("/:id")
  .get(readSpecificUserController)
  .patch(updateUserController)
  .delete(deleteUserController);

userRouter.route("/login").post(loginUserController);
export default userRouter;
