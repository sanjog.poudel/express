import { secretKey } from "../constant.js";
import { Login } from "../schema/model.js";
import { sendEmail } from "../utils/sendMail.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export const createLogin = async (req, res) => {
  try {
    let data = req.body;

    let hashPassword = await bcrypt.hash(data.password, 10);

    data = {
      ...data,
      isVerifiedEmail: false,
      password: hashPassword,
    };

    let result = await Login.create(data);

    //send email with link
    //generate token

    let infoObj = {
      _id: result._id,
    };

    let expireInfo = {
      expiresIn: "5d",
    };

    let token = await jwt.sign(infoObj, secretKey, expireInfo);

    //send mail
    await sendEmail({
      from: "'Main'<sin.musuc21@gmail.com>",
      to: [data.email],
      subject: "account created",
      html: `
      <h1> your account has been created successfully</h1>
      <a href=http://localhost:3000/verify-email?token=${token}>
      http://localhost:3000/verify-email?token=${token}
      </a>
      `,
    });

    res.json({
      success: true,
      message: "user created successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const verifyEmail = async (req, res, next) => {
  try {
    let tokenString = req.headers.authorization;
    // console.log(tokenString.split(" ")[1]);
    let tokenArray = tokenString.split(" ");
    let token = tokenArray[1];
    console.log(token);

    //verify token

    let infoObj = await jwt.verify(token, secretKey);
    let userId = infoObj._id;

    let result = await Login.findByIdAndUpdate(
      userId,
      {
        isVerifiedEmail: true,
      },
      {
        new: true,
      }
    );

    res.json({
      success: true,
      message: "Login verified successfully.",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export const LoginUser = async (req, res, next) => {
  try {
    let email = req.body.email;
    let password = req.body.password;

    let user = await Login.findOne({ email: email });
    if (user) {
      if (user.isVerifiedEmail) {
        let isValidPassword = await bcrypt.compare(password, user.password);

        if (isValidPassword) {
          let infoObj = {
            _id: user._id,
          };

          let expireInfo = {
            expiresIn: "5d",
          };

          let token = await jwt.sign(infoObj, secretKey, expireInfo);

          res.json({
            success: true,
            message: "user login successful",
            data: token,
          });
        } else {
          let error = new Error("credential does not match");
          throw error;
        }
      } else {
        let error = new Error("credential does not match");
        throw error;
      }
    } else {
      let error = new Error("credential does not found");
      throw error;
    }
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllLogin = async (req, res, next) => {
  try {
    let result = await readALlLogin({});
    res.status(200).json({
      success: true,
      message: "login read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificLogin = async (req, res, next) => {
  try {
    let result = await readSpecificLogin(req.params.id);

    res.status(200).json({
      success: true,
      message: "login read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: "error message",
    });
  }
};
export let updateLogin = async (req, res, next) => {
  try {
    let result = await updateLogin(req.params.id, req.body);
    res.status(201).json({
      success: true,
      message: "login update successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteLogin = async (req, res, next) => {
  try {
    let result = await deleteLogin(req.params.id);
    res.status(200).json({
      success: true,
      message: "login deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let loginLogin = async (req, res, next) => {
  let email = req.body.email;
  let password = req.body.password;

  let login = await Login.findOne({ email: email });

  if (login === null) {
    res
      .status(404)
      .json({ success: false, message: "email and password does not match" });
  } else {
    let dbPassword = login.password;
    let isValidPassword = await bcrypt.compare(password, dbPassword);

    if (isValidPassword) {
      res.status(200).json({ success: true, message: "login successfully" });
    } else {
      res
        .status(400)
        .json({ success: false, message: "email and password does not match" });
    }
  }
};
