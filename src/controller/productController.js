import {
  createProductService,
  deleteProductService,
  readALlProductService,
  readSpecificProductService,
  updateProductService,
} from "../service/productService.js";

export let createProductController = async (req, res, next) => {
  try {
    let result = await createProductService(req.body);
    res.status(201).json({
      success: true,
      message: "product created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllProductController = async (req, res, next) => {
  try {
    let result = await readALlProductService({});
    res.status(200).json({
      success: true,
      message: "product read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificProductController = async (req, res, next) => {
  try {
    let result = await readSpecificProductService(req.params.id);

    res.status(200).json({
      success: true,
      message: "product read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: "error message",
    });
  }
};
export let updateProductController = async (req, res, next) => {
  try {
    let result = await updateProductService(req.params.id, req.body);
    res.status(201).json({
      success: true,
      message: "product update successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteProductController = async (req, res, next) => {
  try {
    let result = await deleteProductService(req.params.id);
    res.status(200).json({
      success: true,
      message: "product deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
