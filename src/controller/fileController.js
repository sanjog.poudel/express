export let uploadSingleFile = (req, res, next) => {
  // console.log(req.body);
  let link = `http://localhost:8000/${req.file.filename}`;
  res.json({
    success: true,
    message: "file uploaded successfully",
    result: link,
  });
};

export let uploadMultipleFile = (req, res, next) => {
  // console.log(req.files);

  let link = req.files.map((item) => {
    return `serverUrl/${item.filename}`;
  });
  res.status(201).json({
    success: true,
    message: "multiple file upload successfully.",
    result: link,
  });
};
