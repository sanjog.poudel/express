import { secretKey } from "../../token.js";
import { WebUser } from "../schema/model.js";

import { sendEmail } from "../utils/sendMail.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export let createWebUSerController = async (req, res) => {
  try {
    let data = req.body;
    let hashPassword = await bcrypt.hash(data.password, 10);

    data = {
      ...data,
      isVerifiedEmail: false,
      password: hashPassword,
    };
    let result = await WebUser.create(data);

    let infoObj = {
      _id: result._id,
    };

    let expiryInfo = {
      expiryIn: "5d",
    };

    await sendEmail  ({
      from: "'hello'<sanjog@gmail.com>",
      to: data.email,
      subject: "account create",
      html: `
        <h1>your account has been created successfully /h1>
        
        <a href=" http://localhost:3000/verify-email?token=${token}">
        http://localhost:3000/verify-email?token=${token}
        `,
    }); 

    let token = await jwt.sign(infoObj, secretKey, expiryInfo);

    //send email
    await sendEmail({
      from: "'Houseofjob'<sanjog.poudel@gmail.com",
      to: data.email,
      subject: "account create",
      html: `
      <h1> your account have been created successfully</h1>
      <a href="http:000//localhost:3000.verify-email?token=${token}">
      http://localhost:3000.verify-email?token=${token}
      </a>
      `,
    });

    res.json({
      success: true,
      message: "user created successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
// export let readAllWebUSerController = async (req, res, next) => {
//   try {
//     let result = await readALlWebUSerService({});
//     res.status(200).json({
//       success: true,
//       message: "webUser read successfully",
//       result: result,
//     });
//   } catch (error) {
//     res.status(400).json({
//       success: false,
//       message: error.message,
//     });
//   }
// };
// export let readSpecificWebUSerController = async (req, res, next) => {
//   try {
//     let result = await readSpecificWebUSerService(req.params.id);

//     res.status(200).json({
//       success: true,
//       message: "webUser read successfully",
//       result: result,
//     });
//   } catch (error) {
//     res.status(400).json({
//       success: false,
//       message: "error message",
//     });
//   }
// };
// export let updateWebUSerController = async (req, res, next) => {
//   try {
//     let result = await updateWebUSerService(req.params.id, req.body);
//     res.status(201).json({
//       success: true,
//       message: "webUser update successfully",
//       result: result,
//     });
//   } catch (error) {
//     res.status(400).json({
//       success: false,
//       message: error.message,
//     });
//   }
// };
// export let deleteWebUSerController = async (req, res, next) => {
//   try {
//     let result = await deleteWebUSerService(req.params.id);
//     res.status(200).json({
//       success: true,
//       message: "webUser deleted successfully",
//       result: result,
//     });
//   } catch (error) {
//     res.status(400).json({
//       success: false,
//       message: error.message,
//     });
//   }
// };

// export let loginWebUSerController = async (req, res, next) => {
//   let email = req.body.email;
//   let password = req.body.password;

//   let webUser = await webUSer.findOne({ email: email });

//   if (webUser === null) {
//     res
//       .status(404)
//       .json({ success: false, message: "email and password does not match" });
//   } else {
//     let dbPassword = webUser.password;
//     let isValidPassword = await bcrypt.compare(password, dbPassword);

//     if (isValidPassword) {
//       res.status(200).json({ success: true, message: "login successfully" });
//     } else {
//       res
//         .status(400)
//         .json({ success: false, message: "email and password does not match" });
//     }
//   }
// };
