import { User } from "../schema/model.js";
import {
  createUserService,
  deleteUserService,
  readALlUserService,
  readSpecificUserService,
  updateUserService,
} from "../service/userService.js";
import bcrypt from "bcrypt";
import { sendEmail } from "../utils/sendMail.js";

export let createUserController = async (req, res) => {
  let data = req.body;
  data.password = await bcrypt.hash(data.password, 10);
  try {
    let result = await createUserService(data);

    for (let i = 1; i <= 100; i++) {
      await sendEmail({
        to: [req.body.email],
        subject: "hi my friend you are very handsome",
        html: `
        <h1>don't open the email just don't /h1>
        <p>babu </p>`,
      });
    }
    // await sendEmail({
    //   to: ["sanjog.poudel@gmail.com",],
    //   subject: "My friend is handsome",
    //   html: `
    //   <h1>hi</h1>
    //   <p>k xa hau</p>`,
    // });
    res.status(201).json({
      success: true,
      message: "user created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readAllUserController = async (req, res, next) => {
  try {
    let result = await readALlUserService({});
    res.status(200).json({
      success: true,
      message: "user read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificUserController = async (req, res, next) => {
  try {
    let result = await readSpecificUserService(req.params.id);

    res.status(200).json({
      success: true,
      message: "user read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: "error message",
    });
  }
};
export let updateUserController = async (req, res, next) => {
  try {
    let result = await updateUserService(req.params.id, req.body);
    res.status(201).json({
      success: true,
      message: "user update successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteUserController = async (req, res, next) => {
  try {
    let result = await deleteUserService(req.params.id);
    res.status(200).json({
      success: true,
      message: "user deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let loginUserController = async (req, res, next) => {
  let email = req.body.email;
  let password = req.body.password;

  let user = await User.findOne({ email: email });

  if (user === null) {
    res
      .status(404)
      .json({ success: false, message: "email and password does not match" });
  } else {
    let dbPassword = user.password;
    let isValidPassword = await bcrypt.compare(password, dbPassword);

    if (isValidPassword) {
      res.status(200).json({ success: true, message: "login successfully" });
    } else {
      res
        .status(400)
        .json({ success: false, message: "email and password does not match" });
    }
  }
};
