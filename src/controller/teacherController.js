import {
  createTeacherService,
  deleteTeacherService,
  readALlTeacherService,
  readSpecificTeacherService,
  updateTeacherService,
} from "../service/teacherService.js";

export let createTeacherController = async (req, res, next) => {
  try {
    let result = await createTeacherService(req.body);
    res.status(201).json({
      success: true,
      message: "teacher created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllTeacherController = async (req, res, next) => {
  let { totalData, pageNo, sort, ...query } = req.query;

  let limit = totalData;
  let skip = (Number(pageNo) - 1) * Number(totalData);
  try {
    let result = await readALlTeacherService({});
    res.status(200).json({
      success: true,
      message: "teacher read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let readSpecificTeacherController = async (req, res, next) => {
  try {
    let result = await readSpecificTeacherService(req.params.id);

    res.status(200).json({
      success: true,
      message: "teacher read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: "error message",
    });
  }
};
export let updateTeacherController = async (req, res, next) => {
  try {
    let result = await updateTeacherService(req.params.id, req.body);
    res.status(201).json({
      success: true,
      message: "teacher update successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteTeacherController = async (req, res, next) => {
  try {
    let result = await deleteTeacherService(req.params.id);
    res.status(200).json({
      success: true,
      message: "teacher deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
