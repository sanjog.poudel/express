import { Schema } from "mongoose";

let userSchema = Schema(
  {
    name: {
      required: true,
      type: String,
      trim: true,
    },
    isMarried: {
      type: Boolean,
      trim: true,
    },
    email: {
      unique: true,
      required: true,
      type: String,
      trim: true,
    },
    password: {
      required: true,
      type: String,
      trim: true,
    },
    dob: {
      required: true,
      type: Date,
      trim: true,
    },
    gender: {
      required: true,
      type: String,
      trim: true,
      default: "male",
    },
    address: {
      type: String,
      trim: true,
    },
    phoneNumber: {
      required: true,
      type: Number,
      trim: true,
    },
  },
  { timestamps: true }
);

export default userSchema;
/* 
email => unique
password =>
phoneNumber
gender
dob
isMarried
*/
