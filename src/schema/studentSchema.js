import { Schema } from "mongoose";

let studentSchema = Schema(
  {
    name: {
      type: String,
      // lowercase: true,
      //   uppercase: true,
      //   trim: true,
      //   required: [true, "name field is required"], // error field must be resemble with field name
      //   minLength: [3, "name field must be more than 3 character"],
      //   maxLength: [25, "name filed must be less then 25 character"],
    },
    password: {
      type: String,
    },
    phoneNumber: {
      type: Number,
      // min: [1000000000, "phoneNumber must be of 10 digits"],
      // max: [9999999999, "phoneNumber must be of 10 digits"],
      // validate: (value) => {
      //   if (value.toString().length !== 10) {
      //     throw new Error("phoneNumber must be of 10 digits");
      //   }
      // },
    },
    age: {
      type: Number,
      // min: [400, "roll must be more than 400"],
      // max: [500, "roll must be more than 500"],
    },
    isMarried: {
      type: Boolean,
      default: false,
    },
    spouseName: {
      type: String,
    },
    email: {
      type: String,
    },
    gender: {
      type: String,
      // default: "male",
      // validate: (value) => {
      //   if (value === "male" || value === "female" || value === "other") {
      //   } else {
      //     throw new Error("please enter correct gender");
      //   }
      // },
    },
    dob: {
      type: Date,
      default: new Date(),
    },
    location: {
      country: {
        type: String,
      },
      exactLocation: {
        type: String,
      },
      favTeacher: [{ type: String }],
    },
    favSubject: [
      {
        bookName: {
          type: String,
        },
        bookAuthor: {
          type: String,
        },
      },
    ],
  },
  { timestamps: true }
);

export default studentSchema;
