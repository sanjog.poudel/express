import { model } from "mongoose";
import bookSchema from "./bookSchema.js";
import collegeSchema from "./collegeSchema.js";
import departmentSchema from "./departmentSchema.js";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import traineeSchema from "./traineeSchema.js";
import classroomSchema from "./classroomSchema.js";
import productSchema from "./productSchema.js";
import userSchema from "./userSchema.js";
import reviewSchema from "./reviewSchema.js";
import webUserSchema from "./webUserSchema.js";
import loginSchema from "./loginSchema.js";

export let Student = model("Student", studentSchema);
export let Teacher = model("Teacher", teacherSchema);
export let Book = model("Book", bookSchema);
export let Trainee = model("Trainee", traineeSchema);
export let College = model("College", collegeSchema);
export let ClassRoom = model("ClassRoom", classroomSchema);
export let Department = model("Department", departmentSchema);
export let Product = model("Product", productSchema);
export let User = model("User", userSchema);
export let Review = model("Review", reviewSchema);
export let WebUser = model("WebUser", webUserSchema);
export let Login = model("Login", loginSchema);

/* 
first letter model must be capital and singular
*/
