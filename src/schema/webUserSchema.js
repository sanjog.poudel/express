import { Schema } from "mongoose";

let webUserSchema = Schema(
  {
    fullName: {
      type: String,
      required: [true, "fullName field is required"],
    },
    email: {
      type: String,
      unique: true,
      required: [true, "email field is required"],
    },
    password: {
      type: String,
      required: [true, "password field is required"],
    },
    dob: {
      required: [true, "dob field is required"],
      type: Date,
    },
    gender: {
      type: String,
      required: [true, "gender field is required"],
    },
    role: {
      type: String,
      required: [true, "gender field is required"],
    },
    isVerifiedEmail: {
      type: Boolean,
      required: [true, "isVerifiedEmail  is required"],
    },
  },
  { timestamps: true }
);
export default webUserSchema;
