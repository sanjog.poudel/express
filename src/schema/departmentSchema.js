import { Schema } from "mongoose";

let departmentSchema = Schema({
  name: {
    required: true,
    type: String,
  },
  totalNumber: {
    required: true,
    type: Number,
  },
  hod: {
    required: true,
    type: String,
  },
},
{ timestamps: true });
export default departmentSchema;
