import { Router } from "express";

export let bikeRouter = Router();

bikeRouter
  .route("/")
  /*   .route("/")
  .get((req, res, next) => {
    console.log(req.body); //taking from postman
    res.json("bike get");
  })
  .patch((req, res, next) => {
    console.log(req.bodygit );
    res.json("bike patch");
  })
  .delete((req, res, next) => {
    res.json("bike delete");
  });
 */
  .post(
    (req, res, next) => {
      console.log("i am normal middleware 1");
      let error = new Error("this is my 2 error");
      next(error);
    },
    (error, req, res, next) => {
      console.log("i am error middleware 1");
      console.log(error.message);
    },
    (req, res, next) => {
      console.log("i am normal middleware 2");
      next();
    },
    (error, req, res, next) => {
      console.log("i am error middleware 2");
    },
    (req, res, next) => {
      console.log("i am normal middleware 3");
      res.json({
        success: true,
        message: "bike created successfully",
      });
    }
  );
