import { Trainee } from "../schema/model.js";

export let createTraineeService = async (data) => {
  return await Trainee.create(data);
};
export let readALlTraineeService = async (
  query = {},
  sort = null,
  skip = 0,
  limit = 10
) => {
  return await Trainee.find(query).sort(sort).skip(skip).limit(limit);
};
export let readSpecificTraineeService = async (id) => {
  return await Trainee.findById(id);
};
export let updateTraineeService = async (id, data) => {
  return await Trainee.findByIdAndUpdate(id, data, { new: true });
};
export let deleteTraineeService = async (id) => {
  return await Trainee.findByIdAndDelete(id);
};
