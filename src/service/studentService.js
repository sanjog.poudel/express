import { Student } from "../schema/model.js";

export let createStudentService = async (data = {}) => {
  return await Student.create(data);
};
export let readALlStudentService = async (
  query = {},
  sort = null, // we can not "" bug on mongoose
  skip = 0,
  limit = 10
) => {
  //query = {name:"Sanjog"}
  //return await Student.find({})
  return await Student.find(query).sort(sort).skip(skip).limit(limit);
  //find has control over the object
  //select has control over the object property
  // we can not use -(exclusive) and +(inclusive) simultaneously except id

  // return await Student.find({ name: "Sanjog" }).select(
  //   "name password phoneNumber -_id"
  // return await Student.find({}).select("-password");
};
export let readSpecificStudentService = async (id = "") => {
  return await Student.findById(id);
};
export let updateStudentService = async (id = "", data = {}) => {
  return await Student.findByIdAndUpdate(id, data, { new: true });
};
export let deleteStudentService = async (id = "") => {
  return await Student.findByIdAndDelete(id);
};
