import express, { json } from "express";
import { traineesRouter } from "./src/route/traineesRouter.js";
import { firstRouter } from "./src/route/firstRouter.js";
import { nameRouter } from "./src/route/nameRouter.js";
import { bikeRouter } from "./src/route/bikeRouter.js";
import connectToMongoDb from "./src/connectdb/connectToMongoDb.js";
import studentRouter from "./src/myRoute/studentRouter.js";
import productRouter from "./src/myRoute/productRoute.js";
import userRouter from "./src/myRoute/userRouter.js";
import reviewRouter from "./src/myRoute/reviewRouter.js";
import traineeRouter from "./src/myRoute/traineeRouter.js";
import teacherRouter from "./src/myRoute/teacherRouter.js";
import fileRouter from "./src/myRoute/fileRouter.js";
import webUserRouter from "./src/myRoute/webUserRouter.js";
import loginRouter from "./src/myRoute/loginRouter.js";
import { port } from "./src/constant.js";

let expressApp = express();

connectToMongoDb();

// expressApp.use(
//   (req, res, next) => {
//     console.log("i am application, normal error 1");
//     let error = new Error("i am application error");
//     next(error);
//   },
//   (error, req, res, next) => {
//     console.log("i am application, error middleware 1");
//     console.log(error.message);
//     next();
//   },
//   (req, res, next) => {
//     console.log("i am application, normal middleware 2");
//     next();
//   }
// );
//localhost:8000/one.jpe
expressApp.use(express.static(`./public`));
expressApp.use(json());

expressApp.use("/trainees", traineesRouter);

expressApp.use("/", firstRouter);
expressApp.use("/names", nameRouter);
expressApp.use("/bikes", bikeRouter);
expressApp.use("/students", studentRouter);
expressApp.use("/products", productRouter);
expressApp.use("/users", userRouter);
expressApp.use("/reviews", reviewRouter);
expressApp.use("/trainees", traineeRouter);
expressApp.use("/teachers", teacherRouter);
expressApp.use("/files", fileRouter);
expressApp.use("/web-Users", webUserRouter);
expressApp.use("/logins", loginRouter);
expressApp.listen(port, () => {
  console.log("app is listening at port 8000");
});
