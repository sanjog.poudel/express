/* 
create = post
read = get
update = patch/put
Delete = delete

CRUD is always related to DataBase

middleware
  middleware are the function which has req,res,next
  next is used to trigger another middleware 
we have 2 form of middleware
error middleware(err,req,res,next)=>{}
    to trigger error middleware we have to call next(value)
normal middleware(req,res,next)=>{}
    to trigger normal middleware we have to call next()

    middleware is divided into two parts 
    route middleware
    application middleware


IN Model Convention
first letter model must be capital and singular
variable name and model name must be same 

route convention 
at index it is good to use plural routes


product(name,price,quality)
user (name,address)
schema
model 
service
controller
route
index

status code 
success (2XX)
c 201
r 200
u 201
d 200

failure (4XX)
400


{
    "name": "c",
    "password":"Password@123",
    "phoneNumber": 1111111111,
    "roll": 2,
    "isMarried": false,
    "spouseName": "lkjl",
    "email": "abc@",
    "gender": "male",
    "dob": "2019-2-2",
    "location": {
        "country": "nepal",
        "exactLocation": "gagalphedi"
    },
    "favTeacher": [
        "a",
        "b",
        "c",
        "nitan"
    ],



    "favSubject": [
        {
            "bookName": "javascript",
            "bookAuthor": "nitan"
        },
        {
            "bookName": "b",
            "bookAuthor": "b"
        }
    ],



}

in mongoDB 
  array is called as collection (defining collection is called making model)
  object is called as document (defining document is called making schema)
  it means data are saved in the form of collection of document




.env
 it is the file where we define variables
 we must make .env in root director
 we use uppercase convention 
 every data in .env variable are string so no need to use double quotes
 to get .env variable we must use dotenv package
 if you want .env variable in file first you config dotenv in that file
 to get .env variable use process.env.VARIABLE_NAME
 in .env we store variable like
 port
     backend port
     frontend port
url
     server url 
     client url
credential
    email password
    secret key
email info
     email
     app password

 */
/* 
login management

1)register

2)login

3)my profile

4)my profile update

5)update password

6)forget or reset password

7)Delete password

8)Read all user

9)Read specific user


Register process
      full name
      email
      password
      dob
      gender
      role
      isVerifiedEmail

      
verify email
*/
